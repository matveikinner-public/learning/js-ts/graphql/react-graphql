import { FunctionComponent } from "react";
import { Route, Routes } from "react-router-dom";
import routes from "./BooksModule.routes";

const BooksModule: FunctionComponent = () => {
  return (
    <Routes>
      {routes.map((route) => (
        <Route key={route.path} {...route} />
      ))}
    </Routes>
  );
};

export default BooksModule;
