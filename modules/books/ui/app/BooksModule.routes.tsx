import { lazy } from "react";
import { RouteProps } from "react-router-dom";

const HomePage = lazy(() => import("../pages/books/BooksPage"));

export enum HomeModuleRoutes {
  HOME_ROOT = "/",
}

const routes: RouteProps[] = [
  {
    path: HomeModuleRoutes.HOME_ROOT,
    element: <HomePage />,
  },
];

export default routes;
