import { lazy } from "react";
import { Navigate, RouteProps } from "react-router-dom";

const BooksModule = lazy(() => import("@books/ui/app/BooksModule"));

export enum CoreModuleRoutes {
  REDIRECT = "*",
  BOOKS_MODULE = "/",
}

const routes: RouteProps[] = [
  {
    path: CoreModuleRoutes.BOOKS_MODULE,
    element: <BooksModule />,
  },
  {
    path: CoreModuleRoutes.REDIRECT,
    element: <Navigate replace to={CoreModuleRoutes.BOOKS_MODULE} />,
  },
];

export default routes;
