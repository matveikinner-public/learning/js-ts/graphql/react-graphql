import { FunctionComponent, Suspense } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { v4 as uuid } from "uuid";
import routes from "./CoreModule.routes";

const CoreModule: FunctionComponent = () => {
  return (
    <BrowserRouter>
      <Routes>
        {routes.map(({ element, path }) => (
          <Route key={uuid()} element={<Suspense>{element}</Suspense>} path={path} />
        ))}
      </Routes>
    </BrowserRouter>
  );
};

export default CoreModule;
