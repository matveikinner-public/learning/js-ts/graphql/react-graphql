import { FunctionComponent, ReactNode } from "react";
import { MantineProvider } from "@mantine/core";

const CoreProvider: FunctionComponent<{ children: ReactNode }> = ({ children }) => {
  return (
    <MantineProvider withGlobalStyles withNormalizeCSS theme={{}}>
      {children}
    </MantineProvider>
  );
};

export default CoreProvider;
